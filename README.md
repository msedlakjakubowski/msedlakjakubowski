<!-- markdownlint-disable MD026 --> <!-- No trailing punctuation (or emoji) in headings -->
<!-- markdownlint-disable MD033 --> <!-- No inline HTML -->
<!-- markdownlint-disable MD034 --> <!-- No bare URLs -->

# Hello world :wave:

Hi! My name is Marcin ([how to pronounce it](https://forvo.com/word/marcin/)).
I use he/him or [they/them pronouns](https://my.pronouns.page/are/they/them).
I live in Warsaw, Poland, and this is my [personal README](https://docs.gitlab.com/ee/user/profile/#add-details-to-your-profile-with-a-readme).

I got my education in teaching English as a foreign language, and worked as an English teacher for
five years.
Since 2017 I've worked as a Technical Writer.
I joined GitLab in 2019.

In the meantime, I've volunteered at a non-profit, where I organized youth camps and weekend
workshops, leading educational exercises on human rights, diversity, sustainable development, and
peaceful conflict resolution.
I've also co‑organized PyLight Waw, a beginner‑friendly Python meetup and have given
[talks](https://marcin.s-j.me/talks) on software documentation.

No matter my future job title, I'm a [documentarian](https://www.writethedocs.org/documentarians/),
a person who cares about documentation and communication in the software industry.
I'd like to learn more about software UX and designing experiences in general.

I care about open‑source software, the welfare of humans and other animals, and, broadly speaking,
building bridges not walls.

## Working style :computer:

I prefer a work environment that is structured, and defaults to asynchronous communication, but I enjoy 1:1 syncs when it's more efficient.

I can deal with ambiguity well, but don't enjoy it too much.
I'm an [Asker](https://www.theguardian.com/lifeandstyle/2010/may/08/change-life-asker-guesser) and have a [low level of shame](https://img.ifunny.co/images/fdf45eb2d3906a4bb6b713e1b5ae33d70e4269e54a2b4707c06e9752e27a4267_1.webp), so I'll let you know if I need additional context or help.

I can manage multiple tasks well, but need to block time off for deep work to feel fulfilled in my work.

I work off of to-do items and mentions.
👁 If you want my attention sooner - assign me as a reviewer on your MR rather than only mentioning.

For my first 4 years here, I used to rely email notifications and make heavy use of Gmail labels and filters ([see below](#gmail-configuration)). After a full year of being to-do-only for most projects, I'm very glad I switched.

### Schedule and prioritization

I work best in afternoons (UTC+2), so I often work 11 am - 7 pm.
It syncs nicely with folks in American timezones being online.
It is the time when I am most productive.

Normally my day starts with MR docs reviews, which, depending how many I have, can take a few hours.
Personally, I find it most important to unblock others first, as I know the pain of waiting for someone else before I can do my work.

After reviews, I get to my own work, prioritizing feature docs for the groups I support and
Technical Writing OKRs.

When I have time, I like polishing GitLab UI text, like error messages, empty states, or field descriptions.

## Knowledge of GitLab :fox:

Over my time at GitLab I have gained expertise on some features from the Plan stage, like:

- Issues, epics, and boards
- Quick actions
- Markdown
- Emoji

I'm also good at Git and okay at fixing [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/), and
have often helped people fix messy situations.

## Setup

|                         |                                              |
| ----------------------- | -------------------------------------------- |
| **OS**                  | Fedora Linux (in the past: Manjaro, Lubuntu) |
| **Desktop environment** | GNOME (in the past: XFCE, LXDE)              |
| **Text editor**         | VS Code + vim for occasional terminal edits  |

### Gmail configuration

I've finally moved to GitLab to-dos and now check my email only occasionally.

Back when I used email notifications, this was my over-complicated worflow:

> I use a mix of GTD and Inbox ~~Zero~~ Ten <sup>1</sup>.
> My starting point was this blog post: <https://dansilvestre.com/gtd-gmail/>.
> 
> 🖼 See screenshots:
> 
> - [My Gmail inbox](https://gitlab.com/msedlakjakubowski/msedlakjakubowski/-/blob/main/img/email_inbox.png)
> - [Multiple inboxes config](https://gitlab.com/msedlakjakubowski/msedlakjakubowski/-/blob/main/img/inbox_config.png)
> 
> If you want, you can [download my exported filters](https://gitlab.com/msedlakjakubowski/msedlakjakubowski/-/blob/main/mailFilters.xml), and [import them into your Gmail](https://support.google.com/mail/answer/6579?hl=en#zippy=%2Cexport-a-filter).
> Some of them are specific to my role/team, so delete or adapt them for yourself.
> I also don't know what happens if you import them but don't have the labels they use (let me know when you find out).
>
>
> <sup>1</sup> Inbox Zero is unrealistic and too stressful ¯\\\_(ツ)_/¯
